import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

part 'register_event.dart';

part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(const RegisterInitial()) {
    on<RegisterEvent>((event, emit) {
      if (event is UsernameChanged) {
        emit.call(LoginStateChanged(
            event.username, state.password, state.confirmPassword, state.error));
      }
      if (event is PasswordChanged) {
        emit.call(LoginStateChanged(
            state.username, event.password, state.confirmPassword, state.error));
      }
      if (event is ConfirmPasswordChanged) {
        emit.call(
            LoginStateChanged(state.username, state.password, event.password, state.error));
      }
      if (event is ErrorChanged) {
        emit.call(
            LoginStateChanged(state.username, state.password, state.confirmPassword, event.error));
      }
    });
  }

  changeUsername(String username) => add(UsernameChanged(username));

  changePassword(String password) => add(PasswordChanged(password));

  changeConfirmPassword(String password) =>
      add(ConfirmPasswordChanged(password));

  signUp() async {
    String? error;
    final FirebaseAuth auth = FirebaseAuth.instance;
    final user = await auth
        .createUserWithEmailAndPassword(
            email: state.username, password: state.password)
        .onError((error, stackTrace) {
          error = error.toString();
          throw error;
        });
    if(error != null) {
      add(ErrorChanged(error));
    } else {
      user.user!.sendEmailVerification();
    }
  }
}
