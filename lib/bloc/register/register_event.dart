part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent {
  const RegisterEvent();
}
class UsernameChanged extends RegisterEvent {
  final String username;
  const UsernameChanged(this.username);
  @override
  List<Object> get props => [username];
}
class PasswordChanged extends RegisterEvent {
  final String password;
  const PasswordChanged(this.password);
  @override
  List<Object> get props => [password];
}

class ConfirmPasswordChanged extends RegisterEvent {
  final String password;
  const ConfirmPasswordChanged(this.password);
  @override
  List<Object> get props => [password];
}
class ErrorChanged extends RegisterEvent {
  final String? error;
  const ErrorChanged(this.error);
  @override
  List<Object?> get props => [error];
}

