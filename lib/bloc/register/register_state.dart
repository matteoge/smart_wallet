part of 'register_bloc.dart';

@immutable
abstract class RegisterState {
  final String username;
  final String password;
  final String confirmPassword;
  bool get validatePassword => password == confirmPassword;
  final String? error;


  bool get loginValid => false;

  const RegisterState(this.username, this.password, this.confirmPassword, [this.error]);

  @override
  List<Object> get props => [username, password, confirmPassword];
}

class RegisterInitial extends RegisterState {
  const RegisterInitial() : super('', '', '');
}

class LoginStateChanged extends RegisterState {
  final String username;
  final String password;
  final String confirmPassword;
  final String? error;

  const LoginStateChanged(this.username, this.password, this.confirmPassword, this.error)
      : super(username, password, confirmPassword, error);
}
