part of 'expenses_bloc.dart';

@immutable
abstract class ExpensesState {
  final List<Expense> losses;
  final List<Expense> earnings;
  final List<Expense> savings;

  List<Expense> get dateOrderedExpenses  {
    final notOrderedList = [...losses,...earnings,...savings];
    notOrderedList.sort((a,b) => a.date!.compareTo(b.date!));
    return notOrderedList;
  }
  const ExpensesState(
      {required this.losses, required this.earnings, required this.savings});

  @override
  List<Object> get props => [losses, earnings, savings];
}

class ExpensesInitial extends ExpensesState {
  const ExpensesInitial() : super(losses: const [], earnings: const [], savings: const []);
}

class ExpensesChanged extends ExpensesState {
  final List<Expense> losses;
  final List<Expense> earnings;
  final List<Expense> savings;

  const ExpensesChanged(this.losses, this.earnings, this.savings)
      : super(losses: losses, earnings: earnings, savings: savings,);
}
