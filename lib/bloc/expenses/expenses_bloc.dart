import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

import '../../models/expense_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'expenses_event.dart';

part 'expenses_state.dart';

class ExpensesBloc extends Bloc<ExpensesEvent, ExpensesState> {
  ExpensesBloc() : super(const ExpensesInitial()) {
    on<ExpensesEvent>((event, emit) {
      if (event is ExpensesLoadedEvent) {
        emit.call(ExpensesChanged(
            event.losses, event.earnings, event.savings));
      }
      if (event is LossChangedEvent) {
        emit.call(ExpensesChanged(
            event.losses, state.earnings, state.savings));
      }
      if (event is EarningChangedEvent) {
        emit.call(ExpensesChanged(
            state.losses, event.earnings, state.savings));
      }
      if (event is SavingChangedEvent) {
        emit.call(ExpensesChanged(
            state.losses, state.earnings, event.savings));
      }

    });
  }

  final FirebaseAuth auth = FirebaseAuth.instance;
  late final collection =
      FirebaseFirestore.instance.collection(auth.currentUser!.uid);
  late final losses = collection.doc('losses');
  late final earnings = collection.doc('earnings');
  late final savings = collection.doc('savings');

  loadExpenses() async {
    add(ExpensesLoadedEvent(
        await getList(ExpenseType.loss, losses),
        await getList(ExpenseType.earning, earnings),
        await getList(ExpenseType.saving, savings)));
  }

  Future<List<Expense>> getList(
      ExpenseType type, DocumentReference<Map<String, dynamic>> map) async {
    final dati = await map.get();
    if (dati.data() == null) {
      return [];
    }
    return dati
        .data()!
        .entries
        .map((mapEntry) => Expense.fromFirebase(mapEntry.value))
        .toList();
  }
  addExpense(Expense newExpense, BuildContext context) async {
    switch (newExpense.type) {
      case ExpenseType.loss:
        await addLoss(newExpense);
      case ExpenseType.earning:
        await addEarning(newExpense);
      case ExpenseType.saving:
        await addSaving(newExpense);
    }
    if(context.mounted)Navigator.of(context).pop();
  }
  addLoss(Expense loss) async {
    await loss.loadOnFirebase();
    add(LossChangedEvent([
      loss,
      ...state.losses,
    ]));
  }

  removeLoss(Expense loss) async {
    await loss.deleteOnFirebase();
    add(LossChangedEvent(
      state.losses.where((element) => element.id != loss.id).toList(),
    ));
  }

  addEarning(Expense earning) async {
    await earning.loadOnFirebase();
    add(EarningChangedEvent(
      [
        earning,
        ...state.earnings,
      ],
    ));
  }

  removeEarning(Expense earning) async {
    await earning.deleteOnFirebase();
    add(EarningChangedEvent(
      state.earnings.where((element) => element.id != earning.id).toList(),
    ));
  }

  addSaving(Expense saving) async {
    await saving.loadOnFirebase();
    add(SavingChangedEvent([
      saving,
      ...state.savings,
    ]));
  }

  removeSaving(Expense saving) async {
    await saving.deleteOnFirebase();
    add(SavingChangedEvent(
        state.savings.where((element) => element.id != saving.id).toList()));
  }
}
