part of 'expenses_bloc.dart';

@immutable
abstract class ExpensesEvent {
  const ExpensesEvent();
}

class ExpensesLoadedEvent extends ExpensesEvent {
  final List<Expense> losses;
  final List<Expense> earnings;
  final List<Expense> savings;

  const ExpensesLoadedEvent(this.losses, this.earnings, this.savings);

  @override
  List<Object> get props => [losses, earnings, savings];
}

class LossChangedEvent extends ExpensesEvent {
  final List<Expense> losses;

  const LossChangedEvent(this.losses);

  @override
  List<Object> get props => [losses];
}

class EarningChangedEvent extends ExpensesEvent {
  final List<Expense> earnings;

  const EarningChangedEvent(this.earnings);

  @override
  List<Object> get props => [earnings];
}

class SavingChangedEvent extends ExpensesEvent {
  final List<Expense> savings;

  const SavingChangedEvent(this.savings);

  @override
  List<Object> get props => [savings];
}
