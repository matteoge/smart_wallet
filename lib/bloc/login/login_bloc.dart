import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:untitled1/bloc/expenses/expenses_bloc.dart';

import '../../screens/home_screen.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginInitial()) {
    on<LoginEvent>((event, emit) {
      if (event is UsernameChanged) {
        emit.call(LoginStateChanged(
            event.username, state.password, state.visiblePassword));
      }
      if (event is PasswordChanged) {
        emit.call(LoginStateChanged(
            state.username, event.password, state.visiblePassword));
      }
      if (event is PasswordVisibilityChanged) {
        emit.call(
            LoginStateChanged(state.username, state.password, event.visible));
      }
      if (event is ErrorChanged) {
        emit.call(LoginStateChanged(state.username, state.password,
            state.visiblePassword, event.error));
      }
    });
  }

  login(BuildContext context) async {
    String? error;
    final FirebaseAuth auth = FirebaseAuth.instance;
    await auth
        .signInWithEmailAndPassword(
            email: state.username, password: state.password)
        .onError((e, stackTrace) {
      error = e.toString();
      add(ErrorChanged(error));
      throw e!;
    });
    add(ErrorChanged(error));
    if (state.error == null && context.mounted) {
      await context.read<ExpensesBloc>().loadExpenses();
      if (context.mounted) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (ctx) => const HomeScreen()));
      }
    }
  }

  changeUsername(String username) => add(UsernameChanged(username));

  changePassword(String password) => add(PasswordChanged(password));

  visiblePasswordSwitch(bool visible) =>
      add(PasswordVisibilityChanged(visible));
}
