part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {
  const LoginEvent();
}
class UsernameChanged extends LoginEvent {
  final String username;
  const UsernameChanged(this.username);
  @override
  List<Object> get props => [username];
}
class PasswordChanged extends LoginEvent {
  final String password;
  const PasswordChanged(this.password);
  @override
  List<Object> get props => [password];
}
class PasswordVisibilityChanged extends LoginEvent {
  final bool visible;
  const PasswordVisibilityChanged(this.visible);
  @override
  List<Object> get props => [visible];
}
class ErrorChanged extends LoginEvent {
  final String? error;
  const ErrorChanged(this.error);
  @override
  List<Object?> get props => [error];
}
