part of 'login_bloc.dart';

@immutable
abstract class LoginState {
  final String username;
  final String password;
  final bool visiblePassword;
  final String? error;

  const LoginState(this.username, this.password, this.visiblePassword, [this.error]);

  @override
  List<Object?> get props => [username, password, visiblePassword, error];
}

class LoginInitial extends LoginState {
  const LoginInitial() : super('', '', true);
}

class LoginStateChanged extends LoginState {
  final String username;
  final String password;
  final bool visiblePassword;
  final String? error;

  const LoginStateChanged(this.username, this.password, this.visiblePassword, [this.error])
      : super(username, password, visiblePassword, error);
}
