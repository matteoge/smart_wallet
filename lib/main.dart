import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/bloc/expenses/expenses_bloc.dart';
import 'package:untitled1/screens/login_screen.dart';

import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(BlocProvider(create: (ctx) => ExpensesBloc(), child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Smart Wallet Manager',
      theme: ThemeData(
        scaffoldBackgroundColor: const Color.fromRGBO(38, 0, 77, 1),
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white,
                foregroundColor: Colors.deepPurple,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)))),
          bottomSheetTheme: const BottomSheetThemeData(backgroundColor: Colors.black54),
        textTheme: const TextTheme(bodyLarge: TextStyle(color: Colors.white),
            labelLarge: TextStyle(color: Colors.white, fontSize: 16),
            titleLarge: TextStyle(
                fontSize: 35,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        inputDecorationTheme: InputDecorationTheme(hoverColor: const Color.fromRGBO(204, 153, 0, 1),
            counterStyle: const TextStyle(color: Colors.white),
            suffixIconColor: Colors.white,
            labelStyle: const TextStyle(color: Colors.white),
            contentPadding: const EdgeInsets.all(8),
            border: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(15)),
            focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white, width: 3),
                borderRadius: BorderRadius.circular(15))),
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: LoginScreen(),
    );
  }
}
