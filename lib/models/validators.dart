import 'package:intl/intl.dart';

String? notEmptyValidate(String string, [String label = 'campo']) {
  return string.isEmpty ? 'Inserire ${label.toLowerCase()}' : null;
}

String? validateDate(String string, [label = 'Data']) {
  final emptyValidation = notEmptyValidate(string, label);
  if(emptyValidation != null) return emptyValidation;
  try {
    DateFormat('dd/MM/yyyy').parse(string);
    return null;
  } catch (e) {
    return '$label non scritta correttamente';
  }
}

String? validateDouble(String string, [String label = 'Valore']) {
  final emptyValidation = notEmptyValidate(string, label.toLowerCase());
  if(emptyValidation != null) return emptyValidation;
  try {
    double.parse(string);
    return null;
  } catch (e) {
    return '$label non valido';
  }

}