import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:untitled1/widgets/loss_widget.dart';
import 'package:uuid/uuid.dart';

enum ExpenseType { loss, earning, saving }

extension ExpenseTypeExtension on ExpenseType {
  Widget getWidget(Expense expense) {
    switch (this) {
      case ExpenseType.loss:
        return LossWidget(expense);
      case ExpenseType.earning:
        return EarningWidget(expense);
      case ExpenseType.saving:
        return SavingWidget(expense);
    }
  }

  String getString() {
    switch (this) {
      case ExpenseType.loss:
        return 'losses';
      case ExpenseType.earning:
        return 'earnings';
      case ExpenseType.saving:
        return 'savings';
    }
  }
}

ExpenseType getType(String stringFormat) {
  switch (stringFormat) {
    case 'losses':
      return ExpenseType.loss;
    case 'earnings':
      return ExpenseType.earning;
    default:
      return ExpenseType.saving;
  }
}

class Expense {
  ExpenseType type;
  String? id;
  DateTime? date;
  double value;
  String description;
  String category;

  Widget getWidget() => type.getWidget(this);

  Expense({
    this.type = ExpenseType.loss,
    this.id,
    this.date,
    this.value = 0,
    this.description = '',
    this.category = '',
  }) {
    id ??= const Uuid().v1();
  }

  final auth = FirebaseAuth.instance;
  late final doc = FirebaseFirestore.instance
      .collection(auth.currentUser!.uid!)
      .doc(type.getString());

  Map<String, dynamic> getData() => {
        id!: {
          'type': type.getString(),
          'id': id,
          'date': DateFormat('dd/MM/yyyy').format(date!),
          'value': value,
          'description': description,
          'category': category
        }
      };

  loadOnFirebase() async {
    if ((await doc.get()).data() != null) {
      await doc.update(getData());
    } else {
      await doc.set(getData());
    }
  }

  deleteOnFirebase() async {
    await doc.update({id!: FieldValue.delete()});
  }

  factory Expense.fromFirebase(Map<String, dynamic> map) {
    return Expense(
        type: getType(map['type']),
        id: map['id'],
        date: DateFormat('dd/MM/yyyy').parse(map['date']),
        value: map['value'],
        description: map['description'],
        category: map['category']);
  }
}
