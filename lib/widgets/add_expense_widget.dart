import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:untitled1/bloc/expenses/expenses_bloc.dart';
import 'package:untitled1/models/validators.dart';

import '../models/expense_model.dart';

class AddExpenseWidget extends StatefulWidget {
  @override
  State<AddExpenseWidget> createState() => _AddExpenseWidgetState();
}

class _AddExpenseWidgetState extends State<AddExpenseWidget> {

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController dateController = TextEditingController();
  final Expense newExpense = Expense();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExpensesBloc, ExpensesState>(
      builder: (context, state) {
        return BottomSheet(
          onClosing: () {},
          builder: (ctx) => Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButton(
                        value: newExpense.type,
                        items: const [
                          DropdownMenuItem(
                              value: ExpenseType.loss, child: Text('Spesa')),
                          DropdownMenuItem(
                              value: ExpenseType.earning,
                              child: Text('Guadagno')),
                          DropdownMenuItem(
                              value: ExpenseType.saving,
                              child: Text('Risparmio')),
                        ],
                        onChanged: (type) => setState(() {
                              newExpense.type = type!;
                            })),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration:
                          const InputDecoration(labelText: 'Descrizione spesa'),
                      onSaved: (text) => newExpense.description = text!,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: dateController,
                      validator: (val) => validateDate(val!),
                      onSaved: (val) => setState(() {
                        newExpense.date = DateFormat('dd/MM/yyyy').parse(val!);
                      }),
                      onTap: () => showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now()
                                  .subtract(const Duration(days: 365)),
                              lastDate: DateTime.now())
                          .then((value) {
                        newExpense.date = value!;
                        setState(() {
                          dateController.text =
                              DateFormat('dd/MM/yyyy').format(value!);
                        });
                      }),
                      decoration: const InputDecoration(labelText: 'Data spesa'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: const InputDecoration(labelText: 'Importo'),
                      validator: (val) => validateDouble(val!),
                      onSaved: (text) => newExpense.value = double.parse(text!),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: const InputDecoration(labelText: 'Categoria'),
                      onSaved: (text) => newExpense.category = text!,
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton.icon(
                      onPressed: () async {
                        if(formKey.currentState!.validate()) {
                          formKey.currentState!.save();
                          context.read<ExpensesBloc>().addExpense(newExpense, context);
                        }
                      },
                      icon: const Icon(Icons.add),
                      label: const Text('Aggiungi spesa'),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(ctx).viewInsets.bottom),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
