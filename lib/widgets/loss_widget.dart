import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/bloc/expenses/expenses_bloc.dart';

import '../models/expense_model.dart';

class LossWidget extends StatelessWidget {
  final Expense loss;

  const LossWidget(this.loss, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: () async {await context.read<ExpensesBloc>().removeLoss(loss);},
      tileColor: Colors.red,
      title: Text(loss.description),
      leading: Text(loss.category),
      trailing: Text('€ ${loss.value.toStringAsFixed(2)}'),
    );
  }
}

class EarningWidget extends StatelessWidget {
  final Expense earning;

  const EarningWidget(this.earning, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: Colors.green,
      title: Text(earning.description),
      leading: Text(earning.category),
      trailing: Text('€ ${earning.value.toStringAsFixed(2)}'),
    );
  }
}

class SavingWidget extends StatelessWidget {
  final Expense saving;

  const SavingWidget(this.saving, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: Colors.blue,
      title: Text(saving.description),
      leading: Text(saving.category),
      trailing: Text('€ ${saving.value.toStringAsFixed(2)}'),
    );
  }
}
