// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyDJnOqwZBtM0H9fOTVZckY0z7NGotQf32M',
    appId: '1:998373917406:web:79b97db02ce1d7204d784f',
    messagingSenderId: '998373917406',
    projectId: 'smart-wallet-a3367',
    authDomain: 'smart-wallet-a3367.firebaseapp.com',
    storageBucket: 'smart-wallet-a3367.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyABIXGTvN7326RuQN8mgGSAbhI5p_iDuqk',
    appId: '1:998373917406:android:ece6e6e94e311f7c4d784f',
    messagingSenderId: '998373917406',
    projectId: 'smart-wallet-a3367',
    storageBucket: 'smart-wallet-a3367.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyC_VOtVbP1j6zL7wBoU7m78RYc6TMs8YR0',
    appId: '1:998373917406:ios:b96eecc6e97af0464d784f',
    messagingSenderId: '998373917406',
    projectId: 'smart-wallet-a3367',
    storageBucket: 'smart-wallet-a3367.appspot.com',
    iosClientId: '998373917406-1k6on3n9b4hco4pg3v63ert0j6qhereo.apps.googleusercontent.com',
    iosBundleId: 'com.matteogenna.smartwalletmanager.untitled1',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyC_VOtVbP1j6zL7wBoU7m78RYc6TMs8YR0',
    appId: '1:998373917406:ios:03e246324cfe67654d784f',
    messagingSenderId: '998373917406',
    projectId: 'smart-wallet-a3367',
    storageBucket: 'smart-wallet-a3367.appspot.com',
    iosClientId: '998373917406-ad4osinjujk9t2e8m2spupscn7u30qfq.apps.googleusercontent.com',
    iosBundleId: 'com.matteogenna.smartwalletmanager.untitled1.RunnerTests',
  );
}
