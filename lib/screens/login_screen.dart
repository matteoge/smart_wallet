import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/bloc/expenses/expenses_bloc.dart';
import 'package:untitled1/screens/home_screen.dart';
import 'package:untitled1/screens/register_screen.dart';
import '../bloc/login/login_bloc.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(),
      child: Scaffold(
        body: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 48),
                    child: Text(
                      'Login',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      onChanged: (val) =>
                          context.read<LoginBloc>().changeUsername(val),
                      decoration: const InputDecoration(labelText: 'Username'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      obscureText: state.visiblePassword,
                      onEditingComplete: () async {
                        await context.read<LoginBloc>().login(context);

                      },
                      onChanged: (val) =>
                          context.read<LoginBloc>().changePassword(val),
                      decoration: InputDecoration(
                          labelText: 'Password',
                          suffixIcon: IconButton(
                              onPressed: () => context
                                  .read<LoginBloc>()
                                  .visiblePasswordSwitch(
                                      !state.visiblePassword),
                              icon: Icon(!state.visiblePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 24.0),
                    child: ElevatedButton.icon(
                        onPressed: () async {
                          await context.read<LoginBloc>().login(context);

                        },
                        icon: const Icon(Icons.login),
                        label: const Text('login')),
                  ),
                  if (state.error != null)
                    Text(
                      state.error!,
                      style: const TextStyle(color: Colors.red),
                    ),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (ctx) => RegisterScreen()));
                      },
                      child: const Text('or sign up here'))
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
