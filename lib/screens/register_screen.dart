import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/bloc/register/register_bloc.dart';
import '../bloc/login/login_bloc.dart';

class RegisterScreen extends StatelessWidget {
  RegisterScreen({super.key});

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterBloc(),
      child: Scaffold(
        body: BlocBuilder<RegisterBloc, RegisterState>(
          builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 48),
                      child: Text(
                        'Sign up',
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        onChanged: (val) =>
                            context.read<RegisterBloc>().changeUsername(val),
                        decoration:
                            const InputDecoration(labelText: 'Username'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                          validator: (_) => state.validatePassword
                              ? null
                              : 'Passwords are not equal',
                          onChanged: (val) =>
                              context.read<RegisterBloc>().changePassword(val),
                          decoration: const InputDecoration(
                            labelText: 'Password',
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                          validator: (_) => state.validatePassword
                              ? null
                              : 'Passwords are not equal',
                          onChanged: (val) => context
                              .read<RegisterBloc>()
                              .changeConfirmPassword(val),
                          decoration: const InputDecoration(
                            labelText: 'Confirm password',
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 24.0),
                      child: ElevatedButton.icon(
                          onPressed: () async {
                            if (formKey.currentState!.validate()) {
                              await context.read<RegisterBloc>().signUp();
                              if (state.error == null) {
                                if (context.mounted) {
                                  showDialog(
                                      context: context,
                                      builder: (ctx) => const AlertDialog(
                                            content: Text(
                                                'You received an email in order to verify your account, please check it.'),
                                          )).whenComplete(() => Navigator.of(context).pop());
                                }
                              }
                            }
                          },
                          icon: const Icon(Icons.edit),
                          label: const Text('Sign up')),
                    ),
                    if (state.error != null) Text(state.error!)
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
