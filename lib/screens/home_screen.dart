import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/bloc/expenses/expenses_bloc.dart';
import 'package:untitled1/models/expense_model.dart';
import 'package:untitled1/widgets/add_expense_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExpensesBloc, ExpensesState>(
      builder: (context, state) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () {
              showModalBottomSheet(
                  context: context, builder: (ctx) => AddExpenseWidget());
            },
          ),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Expanded(
                    child: state.dateOrderedExpenses.isEmpty
                        ? const Center(
                            child: Text('Nessuna spesa...'),
                          )
                        : ListView.builder(
                            itemCount: state.dateOrderedExpenses.length,
                            itemBuilder: (ctx, index) => Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: state.dateOrderedExpenses[index]
                                      .getWidget(),
                                ))),
              ],
            ),
          ),
        );
      },
    );
  }
}
